#!/bin/sh

set -eu

. ./salsarc

if [ "$#" -ne 2 ] || [ -z "$1" ] || [ -z "$2" ]; then
  echo "Usage: $0 foo \#debian-bar" >&2
  echo "  where 'foo' is the Salsa project name for which you want to setup irc notifications on channel #debian-bar" >&2
  exit 1
fi

PROJECT_NAME="$1"
IRC_CHANNEL="$2"
IRKER_URL="ruprecht.snow-crash.org"

PROJECT_ID=$(curl --silent -f -XGET --header "PRIVATE-TOKEN: $SALSA_TOKEN" "$SALSA_URL/projects?membership=true&simple=true&search=$PROJECT_NAME" | jq ".[] | select(.name == \"$PROJECT_NAME\") | .id")
if [ -z "$PROJECT_ID" ]; then
    echo "Project $PROJECT_NAME not found among your owned projects on $SALSA_URL service" >&2
    exit 1
else
    echo "Setting up irker integration for $PROJECT_NAME ($PROJECT_ID)"
fi

case $PROJECT_ID in
    ''|*[!0-9]*) echo "$PROJECT_NAME not found using $SALSA_URL service" ;;
    *) curl -XPUT --header "PRIVATE-TOKEN: $SALSA_TOKEN" \
	$SALSA_URL/projects/$PROJECT_ID/services/irker \
	--data "recipients=$IRC_CHANNEL&server_host=$IRKER_URL&colorize_messages=1";
       echo;
       if [ $? -eq 0 ]; then
           echo "All done."
       else
           echo "Something went wrong!"
       fi;;
esac
